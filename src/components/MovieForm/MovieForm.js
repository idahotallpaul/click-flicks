import React, { Component } from "react";
import StarRatings from "react-star-ratings";
import Dropdown from "../Other/Dropdown";
import "./MovieForm.scss";

const initialState = {
  id: "new",
  name: "",
  genre: 0,
  rating: 0
};

let savingTimeout;

class MovieForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.id || initialState.id,
      name: this.props.name || initialState.name,
      genre: this.props.genre || initialState.genre,
      rating: this.props.rating || initialState.rating,
      saving: false
    };
    this.newMovieNameInput = React.createRef();
    this.updateMovie = this.props.updateMovie;
  }

  componentDidMount() {
    if (this.state.id === "new") {
      this.newMovieNameInput.current.focus();
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.name !== this.props.name) {
      this.setState({ name: this.props.name });
    }
    if (prevProps.genre !== this.props.genre) {
      this.setState({ genre: this.props.genre });
    }
    if (prevProps.rating !== this.props.rating) {
      this.setState({ rating: this.props.rating });
    }

    // show a visual indicator that something saved to the list.
    // because you can't see list update on mobile
    if (savingTimeout) {
      clearTimeout(savingTimeout);
      savingTimeout = null;
    }

    savingTimeout = setTimeout(
      function() {
        this.setState({ saving: false });
      }.bind(this),
      1000
    );
  }

  addNewMovie = event => {
    event.preventDefault();
    event.stopPropagation();
    this.props.addMovie(this.state);
    this.setState(initialState);
    this.newMovieNameInput.current.focus();
  };

  deleteMovie = id => {
    // todo: add confirmation modal
    this.props.deleteMovie(this.state.id);
  };

  changeAnyProp = delta => {
    if (this.state.id === "new") {
      this.setState(delta);
    } else {
      this.updateMovie(delta);
      this.setState({
        saving: true
      });
    }
  };

  changeMovieName = event => {
    let delta = { id: this.state.id, name: event.target.value };
    this.changeAnyProp(delta);
  };

  changeMovieGenre = event => {
    let delta = { id: this.state.id, genre: event.target.value };
    this.changeAnyProp(delta);
  };

  changeStars = event => {
    let delta = { id: this.state.id, rating: Number(event.target.value) };
    this.changeAnyProp(delta);
  };

  changeRating = (newRating, name) => {
    let delta = { id: this.state.id, rating: newRating };
    this.changeAnyProp(delta);
  };

  render() {
    let button;
    if (this.state.id === "new") {
      button = (
        <button
          type="submit"
          className="btn btn-primary movie-action movie-action-save"
          onClick={this.addNewMovie}
        >
          <i className="fa fa-plus" />
          <span className="d-md-none ml-2">Submit</span>
        </button>
      );
    } else {
      button = (
        <button
          type="button"
          className="btn btn-sm btn-danger movie-action movie-action-delete"
          onClick={this.deleteMovie}
        >
          <i className="fa fa-times" />
          <span className="d-md-none ml-2">Delete</span>
        </button>
      );
    }

    let formClasses =
      this.state.id === "new" ? "row movie-form big-form" : "row movie-form";

    return (
      <form className={formClasses}>
        <i
          className={
            "fa fa-cloud-upload saving-indicator " +
            (this.state.saving ? "is-saving" : "")
          }
        />
        <div className="col-xs-12 col-md">
          <div className="row no-gutters">
            <div className="col-12 col-sm-12 col-md mb-4 mb-md-0">
              <label className="d-md-none">Movie Title</label>
              <input
                ref={this.newMovieNameInput}
                value={this.state.name}
                onChange={this.changeMovieName}
                type="text"
                placeholder="Movie Name"
                className="form-control d-flex flex-grow-1"
                aria-label="Flick Name"
              />
            </div>
            <div className="col-12 col-sm pl-md-1 mb-4 mb-md-0">
              <label className="d-md-none">Movie Genre</label>
              <Dropdown
                dropdownClasses="form-control custom-select"
                selectedItem={this.state.genre}
                onChange={this.changeMovieGenre}
              />
            </div>
            <div className="col-12 col-sm-auto pl-sm-1 mb-4 mb-md-0">
              <label className="d-md-none">Movie Rating</label>
              <div className="input-group">
                <input
                  type="number"
                  value={this.state.rating}
                  min="0"
                  max="5"
                  className="form-control rating-number-input"
                  aria-label="Flick Rating"
                  onChange={this.changeStars}
                />
                <div className="input-group-append">
                  <span className="input-group-text stars">
                    <StarRatings
                      rating={this.state.rating}
                      starRatedColor="#d9534f"
                      starEmptyColor="#bbb"
                      changeRating={this.changeRating}
                      numberOfStars={5}
                      starDimension="18px"
                      starSpacing="1px"
                      name="rating"
                    />
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-auto">{button}</div>
      </form>
    );
  }
}

export default MovieForm;
