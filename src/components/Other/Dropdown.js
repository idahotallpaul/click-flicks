import React, { Component } from "react";

class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.props.onChange;
  }

  render() {
    let dropdownOptions = [
      { id: 0, label: "Movie Genre" },
      { id: 1, label: "Action" },
      { id: 2, label: "Comedy" },
      { id: 3, label: "Romance" }
    ];

    return (
      <select
        value={this.props.selectedItem}
        className={this.props.dropdownClasses}
        onChange={this.onChange}
      >
        {dropdownOptions.map(option => (
          <option value={option.id} key={option.id}>
            {option.label}
          </option>
        ))}
      </select>
    );
  }
}
export default Dropdown;
