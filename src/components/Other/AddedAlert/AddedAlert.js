import React, { Component } from "react";
import "./AddedAlert.scss";

class AddedAlert extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showAlert: this.props.showAlert || false
    };
  }

  render() {
    let alertClasses = this.props.showAlert
      ? "alert alert-success added-alert is-visible"
      : "alert alert-success added-alert";

    return (
      <div className={alertClasses}>
        <i className="fa fa-check" />
        <span className="ml-2">Item added to list!</span>
      </div>
    );
  }
}
export default AddedAlert;
