import React from "react";

const Footer = () => (
  <footer className="mt-5 mb-5">
    <div className="container-fluid">
      <hr />
      <div className="text-center mt-5">
        <p>
          Submitted to <strong>ClickBank</strong> by
        </p>
        <h5>Paul Terhaar</h5>
        <p>
          <a
            className="btn btn-link btn-sm"
            href="mailto:paul@kingbirdcreative.com"
          >
            paul@kingbirdcreative.com
          </a>
          <br />
          <a href="tel:+208-841-7405">208-841-7405</a>
        </p>
        <a
          className="btn btn-primary btn-sm"
          href="https://bitbucket.org/idahotallpaul/click-flicks"
          target="_new"
        >
          <i className="fa fa-download" />
          <span className="ml-2">Download source code</span>
        </a>
      </div>
    </div>
  </footer>
);

export default Footer;
