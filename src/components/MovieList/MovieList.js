import React, { Component } from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import MovieForm from "../MovieForm/MovieForm";
import AddedAlert from "../Other/AddedAlert/AddedAlert";
import "./MovieList.scss";

const rand = require("random-key");
let alertTimeout;

class MovieList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      movies: [
        {
          id: "asdfasdf",
          name: "Die Hard",
          genre: 2,
          rating: 4
        },
        {
          id: "fhgjdgfjh",
          name: "The Notebook",
          genre: 1,
          rating: 1
        }
      ],
      showAlert: false
    };
  }

  addMovie = movie => {
    const movies = [
      {
        id: rand.generate(),
        name: movie.name,
        genre: movie.genre,
        rating: movie.rating
      },
      ...this.state.movies
    ];
    this.setState({
      movies,
      showAlert: true
    });

    if (alertTimeout) {
      clearTimeout(alertTimeout);
      alertTimeout = null;
    }

    alertTimeout = setTimeout(
      function() {
        //Start the timer
        //After a time, showAlert to false
        this.setState({ showAlert: false });
      }.bind(this),
      1500
    );
  };

  updateMovie = delta => {
    let targetMovie = this.state.movies.find(movie => movie.id === delta.id);
    Object.assign(targetMovie, delta);
    this.setState({ movies: this.state.movies });
  };

  deleteMovie = id => {
    const movies = this.state.movies.filter(movie => movie.id !== id);
    this.setState({ movies });
  };

  render() {
    let listItems = this.state.movies.map(movie => (
      <div className="list-group-item" key={movie.id}>
        <MovieForm
          {...movie}
          updateMovie={this.updateMovie}
          deleteMovie={this.deleteMovie}
        />
      </div>
    ));

    return (
      <section>
        <AddedAlert showAlert={this.state.showAlert} />
        <div className="container-fluid mt-5">
          <header className="text-center mb-5">
            <h1>Add a Flick</h1>
            <h5>Type. Tab. Enter.</h5>
          </header>
          <hr />
          <MovieForm addMovie={this.addMovie} />
          <hr />
          <header className="text-center mt-5 mb-5">
            <h2>Your Flick List</h2>
            <h5>Edit Away. Changes are saved automatically.</h5>
          </header>
          <div className="list-group movie-list">
            <ReactCSSTransitionGroup
              transitionName="movie-list"
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
              transitionAppear={true}
              transitionAppearTimeout={500}
            >
              {listItems}
            </ReactCSSTransitionGroup>
          </div>
        </div>
      </section>
    );
  }
}
export default MovieList;
