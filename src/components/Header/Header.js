import React from "react";
import "./Header.scss";

const Header = () => (
  <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
    <div className="container-fluid">
      <a className="navbar-brand" href="/">
        <strong className="text-danger">Click</strong>
        <span>Flicks</span>
      </a>
    </div>
  </nav>
);

export default Header;
