import React, { Component } from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import Header from "../Header/Header";
import Footer from "../Other/Footer";
import MovieList from "../MovieList/MovieList";
import "./App.scss";

class App extends Component {
  render() {
    return (
      <div className="App">
        <ReactCSSTransitionGroup
          transitionName="app-intro"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
          transitionAppear={true}
          transitionAppearTimeout={500}
        >
          <Header />
          <MovieList />
          <Footer />
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}

export default App;
